package com.tesseract.amell.tesseract;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.*;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    Bitmap img;
    private TessBaseAPI mTess;
    String datapath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = BitmapFactory.decodeResource(getResources(),R.drawable.letras);
        datapath = getFilesDir()+"/tesseract/";

        checkFile(new File(datapath + "tessdata/"));

        String lng = "eng";
        mTess = new TessBaseAPI();
        mTess.init(datapath,lng);

        AppCompatButton OCRbutton = (AppCompatButton) findViewById(R.id.OcrButton);

        OCRbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processImage();
            }
        });


    }

    private void copyFiles(){
        try {
            String fPath = datapath+"/tessdata/eng.traineddata";
            String trainedFile = "tessdata/eng.traineddata";

            AssetManager assetManager = getAssets();

            InputStream inStream = assetManager.open(trainedFile);
            OutputStream outStream = new FileOutputStream(fPath);

            byte[] buffer = new byte[1024];
            int r;

            while ((r = inStream.read(buffer)) != -1){
                outStream.write(buffer,0,r);
            }

            outStream.flush();
            outStream.close();
            inStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkFile(File dir){
        if (!dir.exists() && dir.mkdirs()){
            copyFiles();
        }

        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/eng.traineddata";
            File datafile = new File(datafilepath);
            if (!datafile.exists()) {
                copyFiles();
            }
        }
    }

    public void processImage(){
        String OCResult = "";
        mTess.setImage(img);
        OCResult = mTess.getUTF8Text();
        TextView OCRTextView = (TextView) findViewById(R.id.OcrText);
        Log.d("Texto",OCRTextView.toString());
        OCRTextView.setText(OCResult);
    }

}